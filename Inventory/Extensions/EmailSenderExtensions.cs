using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Inventory.Services;

namespace Inventory.Services
{
    public static class EmailSenderExtensions
    {
        public static Task SendEmailConfirmationAsync(this IEmailSender emailSender, string email, string link = "")
        {
            return emailSender.SendEmailAsync(email, "Your Account Registered", "");
        }

        public static Task SendForgotPasswordMailAsync(this IEmailSender emailSender, string email, string link = "")
        {
            return emailSender.SendEmailAsync(email, "Reset Password", "naveen");
        }
    }
}
