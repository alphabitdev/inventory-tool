﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Inventory.Models;

namespace Inventory.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        //protected override void OnModelCreating(ModelBuilder builder)
        //{
        //    base.OnModelCreating(builder);
        //    // Customize the ASP.NET Identity model and override the defaults if needed.
        //    // For example, you can rename the ASP.NET Identity table names and more.
        //    // Add your customizations after calling base.OnModelCreating(builder);
        //}

        public DbSet<Inventory.Models.ApplicationUser> ApplicationUser { get; set; }

        public DbSet<Inventory.Models.Bill> Bill { get; set; }

        public DbSet<Inventory.Models.BillType> BillType { get; set; }

        public DbSet<Inventory.Models.Branch> Branch { get; set; }

        public DbSet<Inventory.Models.CashBank> CashBank { get; set; }

        public DbSet<Inventory.Models.Currency> Currency { get; set; }

        public DbSet<Inventory.Models.Customer> Customer { get; set; }

        public DbSet<Inventory.Models.CustomerType> CustomerType { get; set; }

        public DbSet<Inventory.Models.GoodsReceivedNote> GoodsReceivedNote { get; set; }

        public DbSet<Inventory.Models.Invoice> Invoice { get; set; }

        public DbSet<Inventory.Models.InvoiceType> InvoiceType { get; set; }

        public DbSet<Inventory.Models.NumberSequence> NumberSequence { get; set; }

        public DbSet<Inventory.Models.PaymentReceive> PaymentReceive { get; set; }

        public DbSet<Inventory.Models.PaymentType> PaymentType { get; set; }

        public DbSet<Inventory.Models.PaymentVoucher> PaymentVoucher { get; set; }

        public DbSet<Inventory.Models.Product> Product { get; set; }

        public DbSet<Inventory.Models.ProductType> ProductType { get; set; }

        public DbSet<Inventory.Models.PurchaseOrder> PurchaseOrder { get; set; }

        public DbSet<Inventory.Models.PurchaseOrderLine> PurchaseOrderLine { get; set; }

        public DbSet<Inventory.Models.PurchaseType> PurchaseType { get; set; }

        public DbSet<Inventory.Models.SalesOrder> SalesOrder { get; set; }

        public DbSet<Inventory.Models.SalesOrderLine> SalesOrderLine { get; set; }

        public DbSet<Inventory.Models.SalesType> SalesType { get; set; }

        public DbSet<Inventory.Models.Shipment> Shipment { get; set; }

        public DbSet<Inventory.Models.ShipmentType> ShipmentType { get; set; }

        public DbSet<Inventory.Models.UnitOfMeasure> UnitOfMeasure { get; set; }

        public DbSet<Inventory.Models.Vendor> Vendor { get; set; }

        public DbSet<Inventory.Models.VendorType> VendorType { get; set; }

        public DbSet<Inventory.Models.Warehouse> Warehouse { get; set; }

        public DbSet<Inventory.Models.UserProfile> UserProfile { get; set; }
        public DbSet<Inventory.Models.Role> Role { get; set; }

        public DbSet<Inventory.Models.tblMailServerSetting> tblMailServerSetting { get; set; }
    }
}
