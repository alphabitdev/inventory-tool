﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Inventory
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
             .ConfigureAppConfiguration((webHostBuilderContext, configurationbuilder) =>
             {
                 var environment = webHostBuilderContext.HostingEnvironment;
                 configurationbuilder.AddJsonFile("appSettings.json", optional: true, reloadOnChange: true);

                 configurationbuilder.AddEnvironmentVariables();
             })
              .UseStartup<Startup>()
               .UseKestrel(o =>
               {
                   o.Limits.KeepAliveTimeout = TimeSpan.FromMinutes(150);
                   o.Limits.MaxRequestBodySize = 52428800;
               })
            .UseUrls("http://localhost:5050");
    }
}
