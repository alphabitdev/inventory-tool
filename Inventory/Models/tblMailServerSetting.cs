﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class tblMailServerSetting
    {
        public long Id { get; set; }
        public string ServerName { get; set; }
        public string AlternateServerName { get; set; }
        public string SenderName { get; set; }
        public string ReplyToAddress { get; set; }
        public string EmailType { get; set; }
        public string Email { get; set; }
        public string FromEmail { get; set; }
        public Nullable<bool> IsTLSEnabled { get; set; }
        public Nullable<int> Port { get; set; }
        public Nullable<bool> IsAuthentication { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }

    }
}
