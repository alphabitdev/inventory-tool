﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class Shipment
    {
        public int ShipmentId { get; set; }
        [Display(Name = "Shipment Number")]
        public string ShipmentName { get; set; }
        //[Display(Name = "Sales Order")]
        //public int SalesOrderId { get; set; }
        public DateTimeOffset ShipmentDate { get; set; }
        [Display(Name = "Shipment Type")]
        public int ShipmentTypeId { get; set; }

       

        [Display(Name = "Warehouse")]
        public int WarehouseId { get; set; }
        [Display(Name = "Full Shipment")]
        public bool IsFullShipment { get; set; } = true;

        [Display(Name = "Quantity")]
        public int Quantity { get; set; }

        [Display(Name = "Customer")]
        public int CustomerId { get; set; }

        [Display(Name = "Operator Of Shipper")] 
        public string OperatorOfShipper { get; set; }


        //[Display(Name = "Customer")]  
        //public string Customer { get; set; }

        //[Display(Name = "Customer Type")]     
        //public string CustomerType { get; set; }
    }
}
