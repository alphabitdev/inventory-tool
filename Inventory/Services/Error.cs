﻿using Inventory.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;

namespace Inventory
{
    public class Error : ExceptionFilterAttribute
    {
        private readonly ApplicationDbContext Dbcontext;
        private readonly ISession session;
        private static IConfiguration _configuration;
        private static IHostingEnvironment _hostingEnvironment;
        private static IHttpContextAccessor _HttpContextAccessor;

        public Error(ApplicationDbContext context, IHttpContextAccessor accessor, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            Dbcontext = context;
             session = accessor.HttpContext.Session;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            _HttpContextAccessor = accessor;
        }

        public override void OnException(ExceptionContext context)
        {
            string strLogText = "";
            Exception ex = context.Exception;

            context.ExceptionHandled = true;
            var objClass = context;
            strLogText += "Message ---" + ex.Message;

            if (ex.InnerException != null)
            {
                strLogText += Environment.NewLine + "Inner Exception is :" + ex.InnerException;
            }

            StreamWriter log;

            string timestamp = DateTime.Now.ToString("d-MMMM-yyyy", new CultureInfo("en-GB"));

          //  IHttpContextAccessor __HttpContextAccessor = new HttpContextAccessor();

            string errorFolder = Path.Combine(_hostingEnvironment.WebRootPath, "ErrorLog");

            if (!System.IO.Directory.Exists(errorFolder))
            {
                System.IO.Directory.CreateDirectory(errorFolder);
            }

            // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
            if (!File.Exists($@"{errorFolder}\Log_{timestamp}.txt"))
            {
                log = new StreamWriter($@"{errorFolder}\Log_{timestamp}.txt");
            }
            else
            {
                log = File.AppendText($@"{errorFolder}\Log_{timestamp}.txt");
            }

            var controllerName = (string)context.RouteData.Values["controller"];
            var actionName = (string)context.RouteData.Values["action"];

            // Write to the file:
            log.WriteLine(Environment.NewLine + DateTime.Now);
            log.WriteLine("------------------------------------------------------------------------------------------------");
            log.WriteLine("Controller Name :- " + controllerName);
            log.WriteLine("Action Method Name :- " + actionName);
            log.WriteLine("------------------------------------------------------------------------------------------------");
            //log.WriteLine(objClass);
            log.WriteLine(strLogText);
            log.WriteLine();

            // Close the stream:
            log.Close();

            if (!_hostingEnvironment.IsDevelopment())
            {
                // do nothing
                return;
            }
            var result = new RedirectToRouteResult(
            new RouteValueDictionary
            {
             {"controller", "Home"}, {"action", "Error"}
            //{"controller", "Home"}, {"action", "Error"},{"message", ex.Message} //pass message in query string parameter
            });

            Microsoft.AspNetCore.Http.SessionExtensions.SetString(_HttpContextAccessor.HttpContext.Session, "ErrorMessage",  ex.Message);
            Microsoft.AspNetCore.Http.SessionExtensions.SetString(_HttpContextAccessor.HttpContext.Session, "ControllerName",  controllerName);
            Microsoft.AspNetCore.Http.SessionExtensions.SetString(_HttpContextAccessor.HttpContext.Session, "ActionName",  actionName );

            // TODO: Pass additional detailed data via ViewData
            context.Result = result;
        }
        public static string GetIPAddress()
        {
            string hostName = Dns.GetHostName();
            IPAddress address = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0];
            return address.ToString();
        }
    }
}