﻿using Inventory.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace Inventory.Services
{
    public class Mail
    {
        private readonly ApplicationDbContext Dbcontext;       

        public Mail(ApplicationDbContext context)
        {
            Dbcontext = context;
        }

        public bool SendMail(string FromEmail, string FromName, string ToEmails, string ReplayTo, string MailBody, string Subject, string CcEmails, string BccEmails)
        {
            return SendMail(FromEmail, FromName, ToEmails, ReplayTo, MailBody, Subject, CcEmails, BccEmails, true);
        }

        public bool SendMail(string FromEmail, string FromName, string ToEmails, string ReplyTo, string MailBody, string Subject, string CcEmails, string BccEmails, bool IsBodyHtml)
        {
            Exception exception;
            string str;
            bool flag;
            try
            {
                var tblMailServerSetting = Dbcontext.tblMailServerSetting.FirstOrDefault();
                if (tblMailServerSetting == null)
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.SenderName))
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.FromEmail))
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.ServerName))
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.Email))
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.Password))
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.Port.ToString()))
                    return false;
                FromName = tblMailServerSetting.SenderName;
                FromEmail = tblMailServerSetting.FromEmail;
                ReplyTo = tblMailServerSetting.ReplyToAddress;

                MailMessage message = new MailMessage();
                if (!string.IsNullOrEmpty(FromName))
                {
                    message.From = new MailAddress(FromEmail, FromName);
                }
                else
                {
                    message.From = new MailAddress(FromEmail);
                }
                if (!string.IsNullOrEmpty(ReplyTo))
                {
                    message.ReplyToList.Add(new MailAddress(ToEmails, ReplyTo));
                }
                if (!string.IsNullOrEmpty(ToEmails))
                {
                    message.To.Add(ToEmails);
                }
                if (!string.IsNullOrEmpty(CcEmails))
                {
                    message.CC.Add(CcEmails);
                }

                if (!string.IsNullOrEmpty(BccEmails))
                {
                    message.Bcc.Add(BccEmails);
                }

                message.Subject = Subject;
                message.Body = MailBody;
                message.IsBodyHtml = IsBodyHtml;
                SmtpClient smtp = new SmtpClient();
                string emailid = string.Empty;
                string password = string.Empty;

                smtp.Host = tblMailServerSetting.ServerName;
                emailid = tblMailServerSetting.Email;
                password = General.Decrypt(tblMailServerSetting.Password);   
                var portNumber = General.ReturnInteger32NullValue(tblMailServerSetting.Port);
             
                message.Priority = MailPriority.High;                

                if (((emailid != "") && (password != "")))
                {
                    NetworkCredential credential = new System.Net.NetworkCredential(emailid, password);
                    smtp.EnableSsl = true;
                    smtp.Credentials = credential;
                    smtp.Port = portNumber;
                }
                else
                {
                    smtp.UseDefaultCredentials = true;
                }

                smtp.Send(message);

                flag = true;
            }
            catch (Exception exception1)
            {
                // InsertError(exception1);
                exception = exception1;
                str = exception.Message.ToString();

                flag = false;
            }
            return flag;
        }


        public bool SendMailAttchments(string FromEmail, string FromName, string ToEmails, string ReplayTo, string MailBody, string Subject, string CcEmails, string BccEmails, string attchment)
        {
            return SendMailAttchments(FromEmail, FromName, ToEmails, ReplayTo, MailBody, Subject, CcEmails, BccEmails, true, attchment);
        }

        public bool SendMailAttchments(string FromEmail, string FromName, string ToEmails, string ReplyTo, string MailBody, string Subject, string CcEmails, string BccEmails, bool IsBodyHtml, string attachment)
        {
            Exception exception;
            string str;
            bool flag;
            try
            {
                var tblMailServerSetting = Dbcontext.tblMailServerSetting.FirstOrDefault();
                if (tblMailServerSetting == null)
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.SenderName))
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.FromEmail))
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.ServerName))
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.Email))
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.Password))
                    return false;
                if (string.IsNullOrEmpty(tblMailServerSetting.Port.ToString()))
                    return false;
                FromName = tblMailServerSetting.SenderName;
                FromEmail = tblMailServerSetting.FromEmail;
                ReplyTo = tblMailServerSetting.ReplyToAddress;

                MailMessage message = new MailMessage();
                if (!string.IsNullOrEmpty(FromName))
                {
                    message.From = new MailAddress(FromEmail, FromName);
                }
                else
                {
                    message.From = new MailAddress(FromEmail);
                }
                if (!string.IsNullOrEmpty(ReplyTo))
                {
                    message.ReplyToList.Add(new MailAddress(ToEmails, ReplyTo));
                }
                if (!string.IsNullOrEmpty(ToEmails))
                {
                    message.To.Add(ToEmails);
                }
                if (!string.IsNullOrEmpty(CcEmails))
                {
                    message.CC.Add(CcEmails);
                }
                if (!string.IsNullOrEmpty(BccEmails))
                {
                    message.Bcc.Add(BccEmails);
                }

                message.Subject = Subject;
                message.Body = MailBody;
                message.Priority = MailPriority.High;
                message.IsBodyHtml = IsBodyHtml;

                //if (attachment.Trim() != "")
                //{
                //    DirectoryInfo diFolder = new DirectoryInfo(attachment);
                //    if (diFolder.Exists)
                //    {
                //        string[] S1 = Directory.GetFiles(attachment);
                //        if (S1.Length > 0)
                //        {
                //            foreach (string fileName in S1)
                //            {

                //                message.Attachments.Add(new Attachment(fileName));
                //            }
                //        }
                //    }
                //}


                if (System.IO.File.Exists(attachment))
                {
                    message.Attachments.Add(new Attachment(attachment));
                }


                //Attachment at = new Attachment(attachment);
                //String Extention = at.Name.Substring(at.Name.LastIndexOf("."));
                //at.Name = "test" + Extention;
                //	message.Attachments.Add(at);

                SmtpClient smtp = new SmtpClient();
              
                string emailid = string.Empty;
                string password = string.Empty;

                smtp.Host = tblMailServerSetting.ServerName;
                emailid = tblMailServerSetting.Email;
                password = General.Decrypt(tblMailServerSetting.Password);
                var portNumber = General.ReturnInteger32NullValue(tblMailServerSetting.Port);

                //smtp.Host = "smtp.gmail.com";
                //emailid = "assetexplorers@gmail.com";
                //password = "asset@567@explorer";

                message.Priority = MailPriority.High;

                if (((emailid != "") && (password != "")))
                {
                    NetworkCredential credential = new System.Net.NetworkCredential(emailid, password);
                    smtp.EnableSsl = true;
                    smtp.Credentials = credential;
                    smtp.Port = portNumber;
                }
                else
                {
                    smtp.UseDefaultCredentials = true;
                }

                smtp.Send(message);
                message.Attachments.Dispose();
                flag = true;
            }
            catch (Exception exception1)
            {
                exception = exception1;
                str = exception.Message.ToString();
                flag = false;
            }
            return flag;
        }

        public static string GetIPAddress()
        {
            string hostName = Dns.GetHostName();
            IPAddress address = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0];
            return address.ToString();
        }
    }
}

