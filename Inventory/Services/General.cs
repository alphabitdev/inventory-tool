﻿using Inventory.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Security.Cryptography;

namespace Inventory.Services
{
    public class General
    {
        #region
        private readonly ApplicationDbContext Dbcontext;
        const string passphrase = "password";
        private static ISession session;
        private static IConfiguration _configuration;        
        private static IHostingEnvironment _hostingEnvironment;       
        private readonly Random _random = new Random();

        #endregion
        public General() { }
        public General(ApplicationDbContext context, IConfiguration configuration, IHostingEnvironment hostingEnvironment, IHttpContextAccessor accessor, Mail mail)
        {
            Dbcontext = context;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            session = accessor.HttpContext.Session;
           
        }

        public static string Encrypt(string message)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(passphrase));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToEncrypt = UTF8.GetBytes(message);
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            string data = Convert.ToBase64String(Results);

            if (data.Contains("/"))
            {
                data = data.Replace("/", "0_0");
            }
            if (data.Contains("+"))
            {
                data = data.Replace("+", "1_1_");
            }
            return data;
        }

        public static string Decrypt(string message)
        {
            if (message.Contains("1_1_"))
            {
                message = message.Replace("1_1_", "+");
            }
            if (message.Contains("0_0"))
            {
                message = message.Replace("0_0", "/");
            }
            try
            {
                byte[] Results;
                System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
                MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
                byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(passphrase));
                TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
                TDESAlgorithm.Key = TDESKey;
                TDESAlgorithm.Mode = CipherMode.ECB;
                TDESAlgorithm.Padding = PaddingMode.PKCS7;
                byte[] DataToDecrypt = Convert.FromBase64String(message);
                try
                {
                    ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                    Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
                }
                finally
                {
                    TDESAlgorithm.Clear();
                    HashProvider.Clear();
                }
                return UTF8.GetString(Results);
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static Int32 ReturnInteger32NullValue(object inputData)
        {
            if (inputData == null && inputData.ToString() == "")
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(inputData);
            }
        }
    }
}