﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Data;
using Inventory.Models;
using Inventory.Models.AccountViewModels;
using Inventory.Models.SyncfusionViewModels;
using Inventory.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Inventory.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Role")]
    public class RoleController : Controller
    {
        private readonly ApplicationDbContext _context;
        public RoleController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Role
        [HttpGet]
        public async Task<IActionResult> GetRole()
        {
            // await _roles.GenerateRolesFromPagesAsync();

            //List<IdentityRole> Items = new List<IdentityRole>();
            //Items = _roleManager.Roles.ToList();
            //int Count = Items.Count();
            //return Ok(new { Items, Count });

            List<Role> Items = await _context.Role.ToListAsync();
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }

        // GET: api/Role
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetRoleByApplicationUserId([FromRoute] string id)
        {
            //long uid = Convert.ToInt32(id);
            //UserProfile userProfile = _context.UserProfile.Where(x => x.UserProfileId == uid).FirstOrDefault();
            //Role userRole = _context.Role.Where(x => x.RoleId == userProfile.UserRole).FirstOrDefault();
            //List<UserRoleViewModel> Items = new List<UserRoleViewModel>();
            //if (userRole != null)
            //{
            //    UserRoleViewModel userRoleViewModel = new UserRoleViewModel();
            //    userRoleViewModel.RoleId = userRole.RoleId;
            //    userRoleViewModel.RoleName = userRole.RoleName;
            //    userRoleViewModel.UserProfileId = userProfile.UserProfileId;

            //    Items.Add(userRoleViewModel);
            //}
            //int Count = Items.Count();
            //return Ok(new { Items, Count });

            List<Role> Items = await _context.Role.ToListAsync();
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Insert([FromBody] CrudViewModel<Role> payload)
        {
            Role role = payload.value;
            _context.Role.Add(role);
            await _context.SaveChangesAsync();
            return Ok(role);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Update([FromBody] CrudViewModel<Role> payload)
        {
            Role role = payload.value;
            Role userRole = _context.Role.Where(u => u.RoleId == payload.value.RoleId).FirstOrDefault();
            userRole.RoleName = role.RoleName;
            _context.Role.Update(userRole);
            await _context.SaveChangesAsync();
            return Ok(userRole);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Remove([FromBody] CrudViewModel<Role> payload)
        {
            var role = _context.Role.Where(u => u.RoleId == (int)payload.key).FirstOrDefault();
            if (role != null)
            {
                _context.Remove(role);
                await _context.SaveChangesAsync();
            }
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateUserRole([FromBody] CrudViewModel<UserRoleViewModel> payload)
        {
            UserRoleViewModel userRole = payload.value;
            if (userRole != null)
            {
                UserProfile userProfile = _context.UserProfile.Where(x => x.UserProfileId == userRole.UserProfileId).FirstOrDefault();
                userProfile.UserRole = payload.value.UserRole;
                _context.UserProfile.Update(userProfile);
                await _context.SaveChangesAsync();
                return Ok(userProfile);

                //var user = await _userManager.FindByIdAsync(userRole.ApplicationUserId);
                //if (user != null)
                //{
                //    if (userRole.IsHaveAccess)
                //    {
                //        await _userManager.AddToRoleAsync(user, userRole.RoleName);
                //    }
                //    else
                //    {
                //        await _userManager.RemoveFromRoleAsync(user, userRole.RoleName);
                //    }
                //}
            }
            return Ok(userRole);
        }
    }
}