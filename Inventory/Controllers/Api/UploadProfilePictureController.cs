﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Inventory.Data;
using Inventory.Models;
using Inventory.Services;

namespace Inventory.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/UploadProfilePicture")]
    [Authorize]
    public class UploadProfilePictureController : Controller
    {
        private readonly IFunctional _functionalService;
        private readonly IHostingEnvironment _env;
        private readonly ApplicationDbContext _context;
        private readonly ISession session;

        public UploadProfilePictureController(IFunctional functionalService, IHttpContextAccessor accessor,
            IHostingEnvironment env,
            ApplicationDbContext context)
        {
            _functionalService = functionalService;
            _env = env;
            _context = context;
            this.session = accessor.HttpContext.Session;
        }

        [HttpPost]
        [RequestSizeLimit(5000000)]
        public async Task<IActionResult> PostUploadProfilePicture(List<IFormFile> UploadDefault)
        {
            try
            {
                var folderUpload = "upload";
                var fileName = "";

                string uid = session.GetString("UserId");

                if (uid != null)
                {
                    long userid = Convert.ToInt32(uid);
                    UserProfile profile = _context.UserProfile.Where(u => u.UserProfileId == userid).FirstOrDefault();

                    if (profile != null)
                    {
                        fileName = await _functionalService.UploadFile(UploadDefault, _env, folderUpload, uid);

                        profile.ProfilePicture = "/" + folderUpload + "/" + fileName;
                        _context.UserProfile.Update(profile);
                        await _context.SaveChangesAsync();
                    }
                }

                return Ok(fileName);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = ex.Message });
            }
        }
    }
}