﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Data;
using Inventory.Models;
using Inventory.Models.AccountViewModels;
using Inventory.Models.SyncfusionViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Inventory.Services;

namespace Inventory.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly ApplicationDbContext _context;
        public UserController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/User
        [HttpGet]
        public IActionResult GetUser()
        {
            List<UserProfile> Items = new List<UserProfile>();
            Items = _context.UserProfile.ToList();
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetByApplicationUserId([FromRoute] string id)
        {
            long uid = Convert.ToInt32(id);
            UserProfile userProfile = _context.UserProfile.Where(x => x.UserProfileId == uid).FirstOrDefault();
            List<UserProfile> Items = new List<UserProfile>();
            if (userProfile != null)
            {
                Items.Add(userProfile);
            }
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Insert([FromBody] CrudViewModel<UserProfile> payload)
        {
            UserProfile register = payload.value;
            if (register.Password.Equals(register.ConfirmPassword))
            {
                register.ApplicationUserId = new Guid().ToString();
                register.Password = General.Encrypt(register.Password);
                _context.UserProfile.Add(register);
                await _context.SaveChangesAsync();
            }
            return Ok(register);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Update([FromBody] CrudViewModel<UserProfile> payload)
        {
            UserProfile profile = payload.value;
            UserProfile user = _context.UserProfile.Where(u => u.UserProfileId == payload.value.UserProfileId).FirstOrDefault();
            user.Email = profile.Email;
            user.FirstName = profile.FirstName;
            user.LastName = profile.LastName;
            _context.UserProfile.Update(user);
            await _context.SaveChangesAsync();
            return Ok(profile);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ChangePassword([FromBody] CrudViewModel<UserProfile> payload)
        {
            UserProfile profile = payload.value;
            if (profile.Password.Equals(profile.ConfirmPassword))
            {
                //var user = await _userManager.FindByIdAsync(profile.ApplicationUserId);
                //var result = await _userManager.ChangePasswordAsync(user, profile.OldPassword, profile.Password);
                UserProfile user = _context.UserProfile.Where(u => u.UserProfileId == payload.value.UserProfileId).FirstOrDefault();
                user.Password = General.Encrypt(profile.Password);
                _context.UserProfile.Update(user);
                await _context.SaveChangesAsync();
                return Ok(user);
            }
            return Ok(profile);
        }

        [HttpPost("[action]")]
        public IActionResult ChangeRole([FromBody] CrudViewModel<UserProfile> payload)
        {
            UserProfile profile = payload.value;
            return Ok(profile);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Remove([FromBody] CrudViewModel<UserProfile> payload)
        {
            var userProfile = _context.UserProfile.Where(u => u.UserProfileId == (int)payload.key).FirstOrDefault();
            if (userProfile != null)
            {
                _context.Remove(userProfile);
                await _context.SaveChangesAsync();
            }
            return Ok();
        }
    }
}