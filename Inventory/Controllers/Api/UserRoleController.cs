﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Data;
using Inventory.Models;
using Inventory.Models.AccountViewModels;
using Inventory.Models.SyncfusionViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/UserRole")]
    public class UserRoleController : Controller
    {
        private readonly ApplicationDbContext _context;
        public UserRoleController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetUserWithRoles()
        {
            List<UserProfile> lstUsers = new List<UserProfile>();
          //  List<UserRoleViewModel> lstUserRoleViewModel = new List<UserRoleViewModel>();

            lstUsers = _context.UserProfile.ToList();

            //foreach (var item in lstUsers)
            //{
            //    UserRoleViewModel userRoleViewModel = new UserRoleViewModel();
            //    userRoleViewModel.Email = item.Email;
            //    userRoleViewModel.FirstName = item.FirstName;
            //    userRoleViewModel.LastName = item.LastName;
            //    userRoleViewModel.UserRole = item.UserRole;
            //    userRoleViewModel.UserProfileId = item.UserProfileId;                          
            //    lstUserRoleViewModel.Add(userRoleViewModel);
            //}
            int Count = lstUsers.Count();
            //  return Ok(new { lstUsers, Count });
            return Json(lstUsers);
        }

    }
}
