﻿using Inventory.Data;
using Inventory.Models;
using Inventory.Models.AccountViewModels;
using Inventory.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Cryptography;

namespace Inventory.Controllers
{
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ISession session;
        //private readonly UserManager<ApplicationUser> _userManager;
        //private readonly SignInManager<ApplicationUser> _signInManager;
        public readonly IConfiguration _configuration;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly General _general;
        const string passphrase = "password";
        public readonly Mail _mail;

        public AccountController(ApplicationDbContext dbContext, IHttpContextAccessor accessor,
            //UserManager<ApplicationUser> userManager,
            //SignInManager<ApplicationUser> signInManager,
            IConfiguration configuration,
            IEmailSender emailSender,
            General general, 
            Mail mail,
            ILogger<AccountController> logger)
        {
            _dbContext = dbContext;
            this.session = accessor.HttpContext.Session;
            //_userManager = userManager;
            //_signInManager = signInManager;
            _configuration = configuration;
            _emailSender = emailSender;
            _general = general;
            _logger = logger;
            _mail = mail;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            session.SetString("UserId", "");
            session.SetString("UserName", "");

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginViewModel model, string returnUrl = null)
        {
            try
            {
                ViewData["ReturnUrl"] = returnUrl;
                if (ModelState.IsValid)
                {
                    var password = General.Encrypt(model.Password);

                    var currentUser = _dbContext.UserProfile.Where(u => u.Email == model.Email && u.Password == password).FirstOrDefault();
                    if (currentUser != null && currentUser.UserProfileId > 0)
                    {
                        _logger.LogInformation("User logged in.");

                        var claims = new List<Claim>
                                {
                                    new Claim("UserId", currentUser.UserProfileId.ToString()),
                                    new Claim("UserName", currentUser.FirstName+ " "+ currentUser.LastName)
                                };

                        HttpContext.SignInAsync(new ClaimsPrincipal(new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme, "UserId", "UserName")));

                        session.SetString("UserId", currentUser.UserProfileId.ToString());
                        session.SetString("UserName", currentUser.FirstName + "" + currentUser.LastName);

                        IHttpContextAccessor _httpContextAccessor = new HttpContextAccessor();
                        Microsoft.AspNetCore.Http.SessionExtensions.SetString(_httpContextAccessor.HttpContext.Session, "UserId", currentUser.UserProfileId.ToString());

                        return RedirectToAction("UserProfile", "UserRole");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Invalid login attempt.");

                        TempData["Error"] = "Invalid login attempt.";


                        return View(model);
                    }
                }
            }
            catch (Exception)
            {
                TempData["Error"] = "Invalid login attempt.";
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new UserProfile { FirstName = model.FirstName, Email = model.Email, Password = model.Password, LastName = model.LastName };
                    Guid guid = new Guid();
                    user.ApplicationUserId = guid.ToString();

                    _dbContext.UserProfile.Add(user);
                    await _dbContext.SaveChangesAsync();

                    _logger.LogInformation("User created a new account with password.");

                    await _emailSender.SendEmailConfirmationAsync(model.Email);

                    _logger.LogInformation("User created a new account with password.");
                    return RedirectToAction("Login");
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "User registration failed");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.Clear();
            await HttpContext.SignOutAsync();
            //await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            TempData["Success"] = "User logged out.";
            return RedirectToAction(nameof(AccountController.Login), "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ConfirmEmail(string userId, string code)
        {
            //if (userId == null || code == null)
            //{
            //    return RedirectToAction(nameof(HomeController.Index), "Home");
            //}
            //var user = await _userManager.FindByIdAsync(userId);
            //if (user == null)
            //{
            //    throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            //}
            //var result = await _userManager.ConfirmEmailAsync(user, code);
            //return View(result.Succeeded ? "ConfirmEmail" : "Error");
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string email = model.Email;
                    var user = _dbContext.UserProfile.Where(u => u.Email.ToString().ToLower() == email.ToLower()).Select(u => u).FirstOrDefault();
                    Guid newGuid = Guid.NewGuid();
                    if (user != null && user.UserProfileId > 0)
                    {
                        var CurrentURL = _configuration.GetSection("appSettings")["CurrentURL"];
                        string redirectUrl = CurrentURL + "Account/ResetPassword?requestId=" + General.Encrypt(user.UserProfileId.ToString()) + "&Id=" + newGuid.ToString();
                        string Messagesubject = "Inventory | Reset Password";
                        StringBuilder mailBody = new StringBuilder();
                        mailBody.Append("Dear, Please follow the directions below to reset your password" + "<br/>");
                        mailBody.Append("<br/>");
                        mailBody.Append("<a href='" + redirectUrl + "' style='Color:blue;font-size:20px;text-decoration:none' target='_blank'><input type='submit' style='color:white;background-color:green' value='Reset Your Password'/></a>" + "<br/>");
                        mailBody.Append("<br/>");
                        mailBody.Append("After you click the button above, you'll be prompted to complete the following steps:" + "<br/>");
                        mailBody.Append("1. Enter your new password" + "<br/>");
                        mailBody.Append("2. Enter confirm password" + "<br/>");
                        mailBody.Append("3. Click Reset Password" + "<br/>");
                        mailBody.Append("Thanks!" + "<br/>");
                        mailBody.Append("Inventory" + "<br/>");
                        mailBody.Append("<br/>");
                        mailBody.Append("<br/>");
                        mailBody.Append("In case you have not done this action, to report it please contact administrator." + "<br/>");
                        mailBody.Append("<br/>");
                        mailBody.Append("<br/>");

                      //  await _emailSender.SendForgotPasswordMailAsync(model.Email);

                        bool b = _mail.SendMail("", "", email, "", mailBody.ToString(), Messagesubject, "", "");    

                        if (b)
                        {
                            user.GuidValue = newGuid.ToString();
                            _dbContext.SaveChanges();
                            _logger.LogInformation("User created a new account with password."); 
                            ModelState.AddModelError(string.Empty, "Mail sent successfully");

                            TempData["Success"] = "Mail sent successfully";
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Sending email failed");
                            TempData["Error"] = "Sending email failed";
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Email is not exists. Please check given email");
                        TempData["Error"] = "Email is not exists. Please check given email";
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError(string.Empty, "Something went wrong");
                    TempData["Error"] = "Something went wrong";
                }               
            }
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string requestId, string Id)
        {
            try
            {
                if (requestId != "" && requestId != null && requestId != " " && Id != "" && Id != null && Id != " ")
                {
                    long rid = Convert.ToInt64(General.Decrypt(requestId));
                    var user = _dbContext.UserProfile.Where(u => u.UserProfileId == rid).Select(u => u).FirstOrDefault();
                    if (user != null && user.UserProfileId > 0)
                    {
                        if (user.GuidValue == Id)
                        {
                            ResetPasswordViewModel enuser = new ResetPasswordViewModel();
                            enuser.UserProfileId = user.UserProfileId;
                            enuser.Email = user.Email;
                            return View(enuser);
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "The link is expired. Please click on Forgot Password again");
                            TempData["Error"] = "The link is expired. Please click on Forgot Password again";
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "User is not exists");
                        TempData["Error"] = "User is not exists";
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "The link is expired. Please click on Forgot Password again");
                    TempData["Error"] = "The link is expired. Please click on Forgot Password again";
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError(string.Empty, "Something went wrong");
                TempData["Error"] = "Something went wrong";
            }

            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ResetPassword(ResetPasswordViewModel model, IFormCollection fc)
        {
            try
            {
                string newPassword = fc["Password"].ToString();
                string hdnUserId = fc["hdnUserId"].ToString();

                if (!string.IsNullOrEmpty(hdnUserId))
                {
                    long uid = Convert.ToInt64(hdnUserId);
                    var user = _dbContext.UserProfile.Where(u => u.UserProfileId == uid).Select(u => u).FirstOrDefault();
                    if (user != null && user.UserProfileId > 0)
                    {
                        user.Password = General.Encrypt(newPassword);
                        Guid newGuid = Guid.NewGuid();
                        user.GuidValue = newGuid.ToString();
                        _dbContext.SaveChanges();

                        SendPasswordResetEmail(user);

                        ModelState.AddModelError(string.Empty, "Password reseted successfully");
                        TempData["Error"] = "Password reseted successfully";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Email is not exists. Please check given email");
                        TempData["Error"] = "Email is not exists. Please check given email";
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Email is not exists. Please check given email");
                    TempData["Error"] = "Email is not exists. Please check given email";
                }

                return RedirectToAction("Login", "Account");
            }
            catch (Exception)
            {
                ModelState.AddModelError(string.Empty, "Something went wrong");
                TempData["Error"] = "Something went wrong";
            }

            return RedirectToAction("Login", "Account");
        }

        private void SendPasswordResetEmail(UserProfile us)
        {
            if (us.Email != "" && us.Email != null)
            {
                string Messagesubject = "Inventory | Your Password Reseted Successfully";
                StringBuilder mailBody = new StringBuilder();
                mailBody.Append("Dear" + " " + us.FirstName + "," + "<br/>");
                mailBody.Append("Greetings from Inventory" + "<br/>");
                mailBody.Append("<br/>");
                mailBody.Append("Your password has been Reseted successfully" + "<br/>");
                mailBody.Append("<br/>");
                mailBody.Append("In case you have not done this action, to report it please contact administrator." + "<br/>");
                mailBody.Append("<br/>");
                mailBody.Append("Thanks!" + "<br/>");
                mailBody.Append("Inventory" + "<br/>");
                mailBody.Append("<br/>");
                mailBody.Append("<br/>");

                bool b = _mail.SendMail("", "", us.Email, "", mailBody.ToString(), Messagesubject, "", "");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }       

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
