﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Data;
using Inventory.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Inventory.Controllers
{
    [Authorize]    
    public class UserRoleController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ISession session;       

        public UserRoleController(ApplicationDbContext dbContext, IHttpContextAccessor accessor)
        {
            _dbContext = dbContext;
            this.session = accessor.HttpContext.Session;          
        }
        
        public IActionResult Index()
        {
            return View();
        }

       
        public IActionResult ChangePassword()
        {
            return View();
        }

       
        public IActionResult Role()
        {
            return View();
        }

    
        public IActionResult ChangeRole()
        {
            return View();
        }      
        public IActionResult UserProfile()
        {
            try
            {
                string uid = session.GetString("UserId");

                if (uid != null)
                {
                    long userid = Convert.ToInt32(uid);
                    UserProfile user = _dbContext.UserProfile.Where(u => u.UserProfileId == userid).FirstOrDefault();
                    if (user != null)
                        return View(user);
                    else
                        return RedirectToAction("Login", "Account");
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch(Exception)
            {
                return RedirectToAction("Login", "Account");
            }
        }
    }
}